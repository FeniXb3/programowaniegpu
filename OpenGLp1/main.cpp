// Triangle.cpp
// Our first OpenGL program that will just draw a triangle on the screen.

#include <Windows.h>
#include <GLTools.h>            // OpenGL toolkit
#include <GL/glut.h>            // Windows FreeGlut equivalent

#include <glFrame.h>
#include <glFrustum.h> 
#include <StopWatch.h> 
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include <vector>

GLuint shader;
GLuint mtrAmbientClrLoc;
GLuint mtrDiffuseClrLoc;
GLuint mtrSpcClrLoc;
GLuint mtrSpcExpLoc;
GLFrustum frustum;
GLFrame cameraFrame;
CStopWatch timer;
const double M_PI = 3.14159265358979323846;

GLint MVPMatrixLocation;
GLint MVMatrixLocation;
GLint normalMatrixLocation;
GLint textureLocation;
GLint lightPositionLocation;
GLint diffuseColorLocation;
GLint ambientColorLocation;
GLint specularColorLocation;
GLint alphaLocation;
GLuint textureID[3];
int n_faces;

void LoadVertices();
void LoadFaces();

void SetTexture(float s, float t)
{
	glVertexAttrib2f(GLT_ATTRIBUTE_TEXTURE0, s, t);
}

GLint GetUniformLocation(GLuint shader_a, const char *name) {
    GLint location = glGetUniformLocation(shader_a, name);
    if (location == -1) {
        fprintf(stderr, "uniform %s could not be found\n", name);
    }
    return location;
};

void DrawPyramid()
{
	// podstawa
	glBegin(GL_QUADS);
		glVertexAttrib3f(GLT_ATTRIBUTE_COLOR, 0.2, 0.2, 0.2);
		glVertex3f(-1.0f,1.0f,0.0f);
		glVertex3f(1.0f,1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,0.0f);
	glEnd();

	// boki
	glBegin(GL_TRIANGLES);
		 // niebieski
		glVertexAttrib3f(GLT_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 2.0f);
		 // zielony
		glVertexAttrib3f(GLT_ATTRIBUTE_COLOR, 0.0, 1.0, 0.0);
		glVertex3f(0.0f, 0.0f, 2.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		 // czerwony
		glVertexAttrib3f(GLT_ATTRIBUTE_COLOR, 1.0, 0.0, 0.0);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 2.0f);
		  // zolty
		glVertexAttrib3f(GLT_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);
		glVertex3f(0.0f, 0.0f, 2.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
	glEnd();
}


///////////////////////////////////////////////////////////////////////////////
// Window has changed size, or has just been created. In either case, we need
// to use the window dimensions to set the viewport and the projection matrix.

void ChangeSize(int w, int h) {
    glViewport(0, 0, w, h);
	frustum.SetPerspective(45.0f,w/h,0.01f,100.0f);
}


///////////////////////////////////////////////////////////////////////////////
// This function does any needed initialization on the rendering context.
// This is the first opportunity to do any OpenGL related tasks.

bool LoadTGATexture(const char *szFileName, GLenum minFilter, GLenum magFilter, GLenum wrapMode) {
    GLbyte *pBits;
    int nWidth, nHeight, nComponents;
    GLenum eFormat;

    // Read the texture bits
    pBits = gltReadTGABits(szFileName, &nWidth, &nHeight, &nComponents, &eFormat);
    if (pBits == NULL)
        return false;

    fprintf(stderr, "read texture from %s %dx%d\n", szFileName, nWidth, nHeight);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, nComponents, nWidth, nHeight, 0,
            eFormat, GL_UNSIGNED_BYTE, pBits);

    free(pBits);

    if (minFilter == GL_LINEAR_MIPMAP_LINEAR ||
            minFilter == GL_LINEAR_MIPMAP_NEAREST ||
            minFilter == GL_NEAREST_MIPMAP_LINEAR ||
            minFilter == GL_NEAREST_MIPMAP_NEAREST)
        glGenerateMipmap(GL_TEXTURE_2D);

    return true;
}

void SetupRC() {
    // Blue background
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    shader = gltLoadShaderPairWithAttributes("pass_thru_shader.vp", "pass_thru_shader.fp",            4,
            GLT_ATTRIBUTE_VERTEX, "vVertex",
            GLT_ATTRIBUTE_COLOR, "vColor",
            GLT_ATTRIBUTE_TEXTURE0, "texCoord0",
            GLT_ATTRIBUTE_NORMAL, "vNormal");
    fprintf(stdout, "GLT_ATTRIBUTE_VERTEX : %d\nGLT_ATTRIBUTE_COLOR : %d \n",
            GLT_ATTRIBUTE_VERTEX, GLT_ATTRIBUTE_COLOR);

	glGenTextures(3, textureID);
    glBindTexture(GL_TEXTURE_2D, textureID[0]);
    if (!LoadTGATexture("ground_1024_Q3.tga", GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE)) {
        fprintf(stderr, "error loading texture\n");
    }
	
    MVPMatrixLocation = GetUniformLocation(shader, "MVPMatrix");
    textureLocation = GetUniformLocation(shader, "texture0");

    lightPositionLocation = GetUniformLocation(shader, "lightPosition");
    diffuseColorLocation = GetUniformLocation(shader, "diffuseColor");
    ambientColorLocation = GetUniformLocation(shader, "ambientColor");
    specularColorLocation = GetUniformLocation(shader, "specularColor");
    MVMatrixLocation = GetUniformLocation(shader, "MVMatrix");
    normalMatrixLocation = GetUniformLocation(shader, "normalMatrix");
    alphaLocation=GetUniformLocation(shader, "alpha");

	if(MVPMatrixLocation==-1)
	{
		fprintf(stderr,"uniform MVPMatrix could not be found\n");
	}

	LoadVertices();
	LoadFaces();
}


void SetUpFrame (GLFrame &frame,const M3DVector3f origin, const M3DVector3f forward, const M3DVector3f up) 
{
	frame.SetOrigin(origin);
	frame.SetForwardVector(forward);
	M3DVector3f side,oUp;
	m3dCrossProduct3(side,forward,up);
	m3dCrossProduct3(oUp,side,forward);
	frame.SetUpVector(oUp);
	frame.Normalize();
}

void LookAt(GLFrame &frame, const M3DVector3f eye, const M3DVector3f at, const M3DVector3f up)
{
    M3DVector3f forward;
    m3dSubtractVectors3(forward, at, eye);
    SetUpFrame(frame, eye, forward, up);
}


void DrawFloor(){
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0f,1.0f);
	int noOfX =10;
	int noOfY =10;
	int l = 1;
	int x,y;
	for(x=-noOfX;x<noOfX;x=x+l){
		for(y=-noOfY;y<noOfY;y=y+l){
			glBegin(GL_POLYGON);
			
			SetTexture(0.0,1.0);
			glVertex3f(x  ,y+l,0.0f);
			SetTexture(1.0,1.0);
			glVertex3f(x  ,y  ,0.0f);
			SetTexture(1.0,0.0);
			glVertex3f(x+l,y  ,0.0f);
			SetTexture(0.0,0.0);
			glVertex3f(x+l,y+l,0.0f);
			glEnd();	
		}
	}
	glDisable(GL_POLYGON_OFFSET_FILL);
}

void TriangleFace(M3DVector3f a, M3DVector3f b, M3DVector3f c) {
   M3DVector3f normal, bMa, cMa;
   m3dSubtractVectors3(bMa, b, a);
   m3dSubtractVectors3(cMa, c, a);
   m3dCrossProduct3(normal, bMa, cMa);
   m3dNormalizeVector3(normal);
   glVertexAttrib3fv(GLT_ATTRIBUTE_NORMAL, normal);
   glVertex3fv(a);
   glVertex3fv(b);
   glVertex3fv(c);
}

 
float ico_vertices[3 * 12] = {
      0., 0., -0.9510565162951536,
      0., 0., 0.9510565162951536,
      -0.85065080835204, 0., -0.42532540417601994,
      0.85065080835204, 0., 0.42532540417601994,
      0.6881909602355868, -0.5, -0.42532540417601994,
      0.6881909602355868, 0.5, -0.42532540417601994,
      -0.6881909602355868, -0.5, 0.42532540417601994,
      -0.6881909602355868, 0.5, 0.42532540417601994,
      -0.2628655560595668, -0.8090169943749475, -0.42532540417601994,
      -0.2628655560595668, 0.8090169943749475, -0.42532540417601994,
      0.2628655560595668, -0.8090169943749475, 0.42532540417601994,
      0.2628655560595668, 0.8090169943749475, 0.42532540417601994
      };
int ico_faces[3*20]={
      1 ,			 11 ,			 7 ,
      1 ,			 7 ,			 6 ,
      1 ,			 6 ,			 10 ,
      1 ,			 10 ,			 3 ,
      1 ,			 3 ,			 11 ,
      4 ,			 8 ,			 0 ,
      5 ,			 4 ,			 0 ,
      9 ,			 5 ,			 0 ,
      2 ,			 9 ,			 0 ,
      8 ,			 2 ,			 0 ,
      11 ,			 9 ,			 7 ,
      7 ,			 2 ,			 6 ,
      6 ,			 8 ,			 10 ,
      10 ,			 4 ,			 3 ,
      3 ,			 5 ,			 11 ,
      4 ,			 10 ,			 8 ,
      5 ,			 3 ,			 4 ,
      9 ,			 11 ,			 5 ,
      2 ,			 7 ,			 9 ,
      8 ,			 6 ,			 2 };
 
void drawTriangles(int n_faces, float *vertices, int *faces) {
      for (int i = 0; i < n_faces; i++) {
      glBegin(GL_TRIANGLES);
      TriangleFace(vertices + 3 * faces[3 * i], vertices + 3 * faces[3 * i + 1], vertices + 3 * faces[3 * i + 2]);
      glEnd();
      }
      }
 
void drawSmoothTriangles(int n_faces, float *vertices, int *faces)
{
	M3DVector3f normal;
	for (int i = 0; i < n_faces; i++) 
	{
		glBegin(GL_TRIANGLES);
		for(int j=0;j<3;++j) 
		{

			m3dCopyVector3(normal,vertices+3*faces[i*3+j]);
			m3dNormalizeVector3(normal);
			glVertexAttrib3fv(GLT_ATTRIBUTE_NORMAL, normal);
			glVertex3fv(vertices+3*faces[i*3+j]);
      
		}
		glEnd();
	}
}
 


void LoadVertices()
{
	int n_vertices = 0;
	FILE *fvertices=fopen("geode_vertices.dat","r");
   if(fvertices==NULL) {
   fprintf(stderr,"cannot open vertices file for reading\n");
   exit(-1);
   }
   char line[120];
   
   std::vector<float > vertices;
   while(fgets(line,120,fvertices)!=NULL) {
   float x,y,z;
   double norm;
   sscanf(line,"%f %f %f",&x,&y,&z);
  
   norm=x*x+y*y+z*z;
   norm=sqrt(norm);
   n_vertices++;
   vertices.push_back(x);
   vertices.push_back(y);
   vertices.push_back(z);
   vertices.push_back(1.0f);
   vertices.push_back(x/norm);
   vertices.push_back(y/norm);
   vertices.push_back(z/norm);
   }


   
	GLuint vertex_buffer;
	glGenBuffers(1,&vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER,n_vertices*sizeof(float)*7,&vertices[0],GL_STATIC_DRAW);
	if(glGetError()!=GL_NO_ERROR) {
		fprintf(stderr,"error copying vertices\n");
	}
	glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX,4,GL_FLOAT,GL_FALSE,sizeof(float)*7,(const GLvoid *)0);
	glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,sizeof(float)*7,(const GLvoid *)(4*sizeof(float)) );
	glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);


	fprintf(stderr,"nv = %u %u\n",n_vertices,vertices.size());
}

void LoadFaces()
{
	char line[120];
	FILE *ffaces=fopen("geode_faces.dat","r");
   if(ffaces==NULL) {
   fprintf(stderr,"cannot open faces file for reading\n");
   exit(-1);
   }

   std::vector<GLuint> faces;
   while(fgets(line,120,ffaces)!=NULL) {
   GLuint  i,j,k;
   
   if(3!=sscanf(line,"%u %u %u",&i,&j,&k)){
   fprintf(stderr,"error reading faces\n"); 
   exit(-1);
   }
   //fprintf(stderr,"%u %u %u\n",i-1,j-1,k-1);
   n_faces++;
   faces.push_back(i-1);
   faces.push_back(j-1);
   faces.push_back(k-1);
   
   }

   GLuint faces_buffer;
	glGenBuffers(1,&faces_buffer);
	if(glGetError()!=GL_NO_ERROR) {
		fprintf(stderr,"faces_buffer invalid\n");
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,faces_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,n_faces*sizeof(GLuint)*3,&faces[0],GL_STATIC_DRAW);
	if(glGetError()!=GL_NO_ERROR) {
		fprintf(stderr,"error copying faces\n");
	}


 	fprintf(stderr,"nf = %u\n",n_faces);
}


///////////////////////////////////////////////////////////////////////////////
// Called to draw scene



void RenderScene(void) {
	//ustawienie kamery
	M3DMatrix44f mCamera;
	M3DMatrix44f matrix = {	
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, -2.0f, 1.0f
		};
	
	M3DVector3f at = {0,0,0};
	M3DVector3f up = {0,0,1};
	M3DVector3f eye;

	M3DMatrix44f mM;
	M3DMatrix44f mMVP;
	float angle = timer.GetElapsedSeconds()*M_PI;

	eye[0]=6.8f*cos(angle);
	eye[1]=6.0f*sin(angle);
	eye[2]=7.0f; 

    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); 
	
    glBindTexture(GL_TEXTURE_2D, textureID[0]);
    glUseProgram(shader);
	
	LookAt(cameraFrame,eye,at,up);


	GLMatrixStack modelView;
	GLMatrixStack projection;
	GLMatrixStack matrixStack;
	GLGeometryTransform geometryPipeline;
	geometryPipeline.SetMatrixStacks(modelView,projection);

	projection.LoadMatrix(frustum.GetProjectionMatrix());
	modelView.PushMatrix();
	cameraFrame.GetCameraMatrix(mCamera);
	modelView.LoadMatrix(mCamera);
	matrixStack.PushMatrix();

	
	glUniformMatrix4fv(MVMatrixLocation,1,GL_FALSE,geometryPipeline.GetModelViewMatrix());
	glUniformMatrix4fv(MVPMatrixLocation,1,GL_FALSE,geometryPipeline.GetModelViewProjectionMatrix());
	glUniformMatrix3fv(normalMatrixLocation,1,GL_FALSE,geometryPipeline.GetNormalMatrix());

	
	    GLfloat vRed[] = {0.0f, 1.0f, 0.0f, 1.0f};
    glBindTexture(GL_TEXTURE_2D, textureID[0]);
    glUseProgram(shader);

    cameraFrame.GetCameraMatrix(mCamera);

    glUniform4f(diffuseColorLocation, 1.0f, 1.0f, 1.0f, 1.0f);
    glUniform4f(ambientColorLocation, .3f, .30f, 0.30f, 1.0f);
	glUniform4f(specularColorLocation, .4f, .4f, 0.4f, 1.0f);

    M3DVector4f lightPosition={-10.0f,20.0f, 50.0f,1.0f};
    M3DVector4f eyeLightPosition;
    m3dTransformVector4(eyeLightPosition,lightPosition,mCamera);
    glUniform3fv(lightPositionLocation,1,&eyeLightPosition[0] );


    glUniform1i(textureLocation, 0);
	

	DrawFloor();
	
	// linie
	glBegin(GL_LINES);
		for(int i = -10; i <= 10; i++)
		{
			glVertex3f(i, -10.0f, 0.0f);
			glVertex3f(i, 10.f, 0.0f);
			
			glVertex3f(-10.0f, i, 0.0f);
			glVertex3f(10.f, i, 0.0f);
		}
	glEnd();

	matrixStack.Translate(10.0f,1.0f,0.0f);
	glUniformMatrix4fv(MVPMatrixLocation,1,GL_FALSE,geometryPipeline.GetModelViewProjectionMatrix());

	//DrawPyramid();
	glDrawElements(GL_TRIANGLES,3*n_faces,GL_UNSIGNED_INT,0);

	matrixStack.PopMatrix();
	matrixStack.Rotate(45.0,0,0,1);
	matrixStack.Translate(0.0,0.0,1.0);
	glUniformMatrix4fv(MVPMatrixLocation,1,GL_FALSE,geometryPipeline.GetModelViewProjectionMatrix());

	//drawSmoothTriangles(20,ico_vertices,ico_faces);
	//DrawPyramid();

	//matrixStack.PopMatrix();
	//matrixStack.PopMatrix();

    // Perform the buffer swap to display back buffer
    glutSwapBuffers();
	glutPostRedisplay();
}



///////////////////////////////////////////////////////////////////////////////
// Main entry point for GLUT based programs

int main(int argc, char* argv[]) {
  

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_STENCIL);
    glutInitWindowSize(600, 600);
    glutCreateWindow("Triangle");
    glutReshapeFunc(ChangeSize);
    glutDisplayFunc(RenderScene);

    GLenum err = glewInit();
    if (GLEW_OK != err) {
        fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
        return 1;
    }

    SetupRC();

    glutMainLoop();
    return 0;
}
